<?php

namespace App\Controller\rest;

use App\BLL\UserBLL;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends BaseApiController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response(
            '',
            Response::HTTP_UNAUTHORIZED
        );
    }

    /**
     * @Route("/auth/login/google")
     */
    public function googleLogin(
        Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        if (is_null($data['access_token'])
            || !isset($data['access_token'])
            || empty($data['access_token']))
            throw new BadRequestHttpException('No se ha recibido el token de google');

        $googleJwt = json_decode(file_get_contents(
            "https://www.googleapis.com/plus/v1/people/me?access_token=" .
            $data['access_token']));

        $token = $userBLL->getTokenByEmail($googleJwt->emails[0]->value);

        return $this->getResponse(['token' => $token]);
    }
}