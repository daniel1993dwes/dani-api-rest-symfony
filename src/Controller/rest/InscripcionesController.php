<?php
/**
 * Created by PhpStorm.
 * User: dani_
 * Date: 21/02/2019
 * Time: 1:53
 */

namespace App\Controller\rest;

use App\BLL\InscripcionesBLL;
use App\Entity\Inscripciones;
use App\Entity\Post;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InscripcionesController extends BaseApiController
{
    /**
     * @Route(
     *      "/inscripciones/{id}.{_format}",
     *      name="new_inscripcion",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"POST"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function nuevo(
        Request $request,
        InscripcionesBLL $inscripcionesBLL,
        Post $post)
    {
        $data = $this->getContent($request);

        $inscripcion = $inscripcionesBLL->nuevo(
            $data['email'], $post);

        return $this->getResponse($inscripcion, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/inscripciones/edit/{id}.{_format}",
     *      name="new_inscripcion",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"POST"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function modificar(
        Request $request,
        InscripcionesBLL $inscripcionesBLL,
        Inscripciones $inscripciones)
    {
        $data = $this->getContent($request);

        $inscripcion = $inscripcionesBLL->modificar(
            $data['email'], $inscripciones);

        return $this->getResponse($inscripcion, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/inscripciones/post/{id}.{_format}",
     *      name="get_inscripciones_post_filter",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function conseguirTodasLasInscripcionesDeUnPost(
        InscripcionesBLL $inscripcionesBLL,
        Post $post)
    {
        $inscripcion = $inscripcionesBLL->postInscripciones(
            $post);

        return $this->getResponse($inscripcion, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/inscripciones/{id}.{_format}",
     *      name="get_one_inscripcion",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function getOne(
        Inscripciones $inscripcion,
        InscripcionesBLL $inscripcionesBLL)
    {
        return $this->getResponse($inscripcionesBLL->toArray($inscripcion));
    }

    /**
     * @Route(
     *      "/inscripciones/user/{id}.{_format}",
     *      name="get_inscripciones_user",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getInscripcionesUser(
        User $user,
        InscripcionesBLL $inscripcionesBLL)
    {
        $inscripciones = $inscripcionesBLL->findBy($user);
        return $this->getResponse($inscripciones);
    }

    /**
     * @Route(
     *      "/inscripciones.{_format}",
     *      name="get_all_inscripciones",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getAll(InscripcionesBLL $inscripcionesBLL)
    {
        $inscripciones = $inscripcionesBLL->getAll();
        return $this->getResponse($inscripciones);
    }

    /**
     * @Route(
     *      "/inscripciones/user.{_format}",
     *      name="get_logger_inscripciones",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function getLoggerUserInscripciones(InscripcionesBLL $inscripcionesBLL)
    {
        $inscripciones = $inscripcionesBLL->findByUserLogger();
        return $this->getResponse($inscripciones);
    }

    /**
     * @Route(
     *      "/inscripciones/{id}.{_format}",
     *      name="delete_inscripcion",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"DELETE"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteInscripcion(Inscripciones $inscripciones, InscripcionesBLL $inscripcionesBLL)
    {
        $inscripcionesBLL->delete($inscripciones);
        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }
}