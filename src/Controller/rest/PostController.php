<?php
/**
 * Created by PhpStorm.
 * User: dani_
 * Date: 21/02/2019
 * Time: 1:53
 */

namespace App\Controller\rest;

use App\BLL\PostBLL;
use App\Entity\Post;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends BaseApiController
{
    /**
     * @Route(
     *      "/posts.{_format}",
     *      name="new_post",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"POST"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function nuevoPost(Request $request, PostBLL $postBLL)
    {
        $data = $this->getContent($request);

        $avatars_directory = $this->getParameter('avatars_directory');

        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        if (is_null($data['imagen']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $post = $postBLL->nuevo(
            $data['titulo'],
            $data['imagen'],
            $data['texto'],
            $data['tipo'],
            $data['aforo'],
            $data['visible'],
            $request,
            $avatars_directory,
            $url_avatars_directory);

        return $this->getResponse($post, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/posts/edit/{id}.{_format}",
     *      name="edit_post",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"POST"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function modificar(Request $request, PostBLL $postBLL, Post $post)
    {
        $data = $this->getContent($request);

        $avatars_directory = $this->getParameter('avatars_directory');

        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        if (is_null($data['imagen']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $post = $postBLL->modificar(
            $post,
            $data['titulo'],
            $data['imagen'],
            $data['texto'],
            $data['tipo'],
            $data['aforo'],
            $data['visible'],
            $request,
            $avatars_directory,
            $url_avatars_directory);

        return $this->getResponse($post, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/posts/filter.{_format}",
     *      name="get_post_filter",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"POST"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function getPostsFiltrados(Request $request, PostBLL $postBLL)
    {
        $data = $this->getContent($request);

        $post = $postBLL->getPostsFiltrados(
            $data['fecha1'],
            $data['fecha2'],
            $data['tipo'],
            $data['texto'],
            $data['orderField'],
            $data['order']);

        return $this->getResponse($post, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/posts.{_format}",
     *      name="get_all_posts",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getAll(PostBll $postBLL)
    {
        $posts = $postBLL->getAll();
        return $this->getResponse($posts);
    }

    /**
     * @Route(
     *      "/posts/{id}.{_format}",
     *      name="get_post",
     *      defaults={"_format": "json"},
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function getOne(Post $post, PostBLL $postBLL)
    {
        return $this->getResponse($postBLL->toArray($post));
    }

    /**
     * @Route(
     *      "/posts/user.{_format}",
     *      name="get_user_logger_posts",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_USER')")
     */
    public function getLoggerUserPosts(PostBLL $postBLL)
    {
        $posts = $postBLL->findByUserLogger();
        return $this->getResponse($posts);
    }

    /**
     * @Route(
     *      "/posts/user/{id}.{_format}",
     *      name="get_user_posts",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getUserPosts(User $user, PostBLL $postBLL)
    {
        $posts = $postBLL->findBy($user);
        return $this->getResponse($posts);
    }

    /**
     * @Route(
     *      "/posts/{id}.{_format}",
     *      name="delete_post",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"DELETE"}
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deletePost(Post $post, PostBLL $postBLL)
    {
        $postBLL->delete($post);
        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }
}