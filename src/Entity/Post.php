<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $comment_cant;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo Titulo no puede estar vacío")
     * @Assert\Length(
     *     min=4,
     *     exactMessage="El título debe tener como mínimo 4 caracteres"
     * )
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="El campo Imagen no puede estar vacío")
     */
    private $imagen;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="El campo Texto no puede estar vacío")
     */
    private $texto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(message="El campo Password no puede estar vacío")
     * @Assert\GreaterThan(
     *     value = 2
     * )
     */
    private $aforo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn()
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;

//    /**
//     * @ORM\OneToMany(targetEntity="App\Entity\Inscripciones", mappedBy="post")
//     */
//    private $inscripciones;
//
//    public function __construct()
//    {
//        $this->inscripciones = new ArrayCollection();
//    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Post
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentCant(): ?int
    {
        return $this->comment_cant;
    }

    public function setCommentCant(int $comment_cant): self
    {
        $this->comment_cant = $comment_cant;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getTexto(): ?string
    {
        return $this->texto;
    }

    public function setTexto(string $texto): self
    {
        $this->texto = $texto;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getAforo(): ?int
    {
        return $this->aforo;
    }

    public function setAforo(?int $aforo): self
    {
        $this->aforo = $aforo;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getUrlImagen()
    {
        return '/images/gallery/'.$this->getImagen();
    }

//    /**
//     * @return Collection|Inscripciones[]
//     */
//    public function getInscripciones(): Collection
//    {
//        return $this->inscripciones;
//    }
//
//    public function addInscripcione(Inscripciones $inscripcione): self
//    {
//        if (!$this->inscripciones->contains($inscripcione)) {
//            $this->inscripciones[] = $inscripcione;
//            $inscripcione->setPost($this);
//        }
//
//        return $this;
//    }
//
//    public function removeInscripcione(Inscripciones $inscripcione): self
//    {
//        if ($this->inscripciones->contains($inscripcione)) {
//            $this->inscripciones->removeElement($inscripcione);
//            // set the owning side to null (unless already changed)
//            if ($inscripcione->getPost() === $this) {
//                $inscripcione->setPost(null);
//            }
//        }
//
//        return $this;
//    }
}
