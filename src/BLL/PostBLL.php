<?php
/**
 * Created by PhpStorm.
 * User: dani_
 * Date: 21/02/2019
 * Time: 1:54
 */

namespace App\BLL;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PostBLL extends BaseBLL
{
    public function nuevo(
        string $titulo, $imagen, string $texto, string $tipo, int $aforo, bool $visible,
        Request $request, $avatars_directory, $url_avatars_directory)
    {
        $post = new Post();
        $post->setTitulo($titulo);
        $post->setTexto($texto);
        $post->setTipo($tipo);
        $post->setAforo($aforo);
        $post->setVisible($visible);
        $post->setUser($this->getUser());
        $post->setCommentCant('0');
        $post->setFecha(new \DateTime());

        $urlImagen = $this->subirImagen($request, $imagen, $avatars_directory, $url_avatars_directory);

        $post->setImagen($urlImagen);

        return $this->guardaValidando($post);
    }

    public function getPostsFiltrados(string $fecha1, string $fecha2, string $tipo, string $texto, string $orderField, string $order)
    {
        $user = $this->getUser();
        $posts = $this->em->getRepository(Post::class)->filtrosComplejos(
            $fecha1, $fecha2, $tipo, $texto, $user, $orderField, $order);
        return $this->entitiesToArray($posts);
    }

    public function subirImagen(
        Request $request,
        $avatar,
        string $avatars_directory,
        string $url_avatars_directory)
    {
        $arr_avatar = explode(',', $avatar);

        if (count($arr_avatar) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imgAvatar = base64_decode($arr_avatar[1]);
        if (!is_null($imgAvatar))
        {
            $fileName = 'evento-'.time().'.jpg';
            $filePath = $url_avatars_directory . $fileName;
            $urlAvatar = $request->getUriForPath($filePath);
            $ifp = fopen($avatars_directory . $fileName, "wb");
            if ($ifp)
            {
                $ok = fwrite($ifp, $imgAvatar);
                if ($ok)
                {
                    fclose($ifp);

                    return $urlAvatar;
                }
            }
        }

        return 'http:/127.0.0.1:8000/uploads/default.jpg';
    }

    public function modificar(
        Post $post, string $titulo, string $imagen, string $texto, string $tipo, int $aforo, bool $visible,
        Request $request, string $avatars_directory, string $url_avatars_directory)
    {
        $post->setTitulo($titulo);
        $post->setTexto($texto);
        $post->setTipo($tipo);
        $post->setAforo($aforo);
        $post->setVisible($visible);
        $post->setUser($this->getUser());
        $post->setCommentCant('0');
        $post->setFecha(new \DateTime());

        $urlImagen = $this->subirImagen($request, $imagen, $avatars_directory, $url_avatars_directory, $post);

        $post->setImagen($urlImagen);

        return $this->guardaValidando($post);
    }

    public function getAll()
    {
        $posts = $this->em->getRepository(Post::class)
            ->findAll();
        return $this->entitiesToArray($posts);
    }

    public function findBy(User $user)
    {
        $posts = $this->em->getRepository(Post::class)
            ->findByUsuario($user);
        return $this->entitiesToArray($posts);
    }

    public function findByUserLogger()
    {
        $user = $this->getUser();

        $posts = $this->em->getRepository(Post::class)
            ->findByUsuario($user);
        return $this->entitiesToArray($posts);
    }

    public function delete(Post $post)
    {
        $this->deleteEntity($post);
    }

    public function toArray($post)
    {
        if (is_null($post))
            return null;

        if (!($post instanceof Post))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $post->getId(),
            'user_id' => $post->getUser(),
            'comment_cant' => $post->getCommentCant(),
            'titulo' => $post->getTitulo(),
            'imagen' => $post->getImagen(),
            'texto' => $post->getTexto(),
            'tipo' => $post->getTipo(),
            'aforo' => $post->getAforo(),
            'fecha' => $post->getFecha(),
            'visible' => $post->getVisible()
        ];
    }
}