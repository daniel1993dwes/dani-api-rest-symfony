<?php
/**
 * Created by PhpStorm.
 * User: dani_
 * Date: 21/02/2019
 * Time: 1:54
 */

namespace App\BLL;

use App\Entity\Inscripciones;
use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class InscripcionesBLL extends BaseBLL
{
    public function nuevo(string $email, Post $post)
    {
        $inscripcion = new Inscripciones();
        $inscripcion->setEmail($email);
        $inscripcion->setFecha(new \DateTime());
        $inscripcion->setUser($this->getUser());
        $inscripcion->setPost($post);
        $inscripcion->setUsername($this->getUser()->getUsername());

        return $this->guardaValidando($inscripcion);
    }

    public function modificar(string $email, Inscripciones $inscripciones)
    {
        $inscripciones->setEmail($email);
        return $this->guardaValidando($inscripciones);
    }

    public function findBy(User $user)
    {
        $inscripciones = $this->em->getRepository(Inscripciones::class)
            ->findByUsuario($user);
        return $this->entitiesToArray($inscripciones);
    }

    public function postInscripciones(Post $post)
    {
        $inscripciones = $this->em->getRepository(Inscripciones::class)
            ->findByPost($post);
        return $this->entitiesToArray($inscripciones);
    }

    public function getAll()
    {
        $inscripciones = $this->em->getRepository(Inscripciones::class)
            ->findAll();
        return $this->entitiesToArray($inscripciones);
    }

    public function findByUserLogger()
    {
        $user = $this->getUser();

        $inscripciones = $this->em->getRepository(Inscripciones::class)
            ->findByUsuario($user);
        return $this->entitiesToArray($inscripciones);
    }

    public function delete(Inscripciones $inscripciones)
    {
        $this->deleteEntity($inscripciones);
    }

    public function toArray($inscripcion)
    {
        if (is_null($inscripcion))
            return null;

        if (!($inscripcion instanceof Inscripciones))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $inscripcion->getId(),
            'user_id' => $inscripcion->getUser(),
            'post_id' => $inscripcion->getPost(),
            'email' => $inscripcion->getEmail(),
            'username' => $inscripcion->getUsername(),
            'fecha' => $inscripcion->getFecha()
        ];
    }
}